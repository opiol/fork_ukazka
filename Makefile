
name=fork

CC=gcc
CFLAGS= -Wall -pedantic -g

OBJ=fork.o
SRC=fork.c
HEAD=fork.h

.PHONY: clean
.PHONY: build
.PHONY: test
.PHONY: trace

build: ${name}

${name}: ${OBJ}
	${CC} ${OBJ} -o ${name} ${CFLAGS}

${OBJ}: ${HEAD}

clean:
	rm -f fork core* *.o

test:
	./${name} /bin/ls ~ -l -i -k --color=auto

trace:
	strace -fe trace=wait4,execve,_exit ./${name} /bin/ls ~ -l

