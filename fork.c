#include "fork.h"

char ***arg;

int main(int argc, char **argv)
{
	char *label = "main";

	pid_t pid;
	int status;

	arg = &argv;


	if ((pid = fork()) == -1) {
		fprintf(stderr, "[%s] Can't fork process!\n", label);
		return EXIT_FAILURE;
	}

	else if (0 == pid) {
		label = "parent";
		print_info(label);
		parent();
	}

	else {
		label = "grandparent";
		print_info(label);
		grandparent();
		if (waitpid(pid, &status, 0) == -1) {
			fprintf(stderr, "[%s] Error while executing waitpid %d.\n", label, pid);
		}
	}


	print_exit_status(label, pid, status);
	return EXIT_SUCCESS;
}

void child()
{
	execv((*arg)[1], *arg + 1 );
}

void parent()
{
	char *label = "parent";
	pid_t pid;
	int status;

	if ((pid = fork()) == -1) {
		fprintf(stderr, "[%s] Can't fork process!\n", label);
	}

	else if (pid == 0) {
		label = "child";
		print_info(label);
		child();
	}

	if (waitpid(pid, &status, 0) == -1) {
		fprintf(stderr, "[%s] Error while executing waitpid22.\n", label);
	}

	print_exit_status(label, pid, status);
}

void grandparent()
{
}


void print_info(const char *label)
{
	printf("%s identification: \n", label);
	printf("    pid = %d,   ppid = %d,  pgrp = %d\n", getpid(), getppid(), getpgrp());
	printf("    uid = %d,   gid = %d\n", getuid(), getgid());
	printf("    euid = %d,  egid = %d\n", geteuid(), getegid());
}

void print_exit_status(const char *label, pid_t pid, int status)
{
	printf("%s exit (pid = %d):", label, pid);
	if (WIFEXITED(status)) {
		printf("    normal termination (exit code = %d)\n", WEXITSTATUS(status));
		return;
	}

	else if (WIFSIGNALED(status)) {
		char *jollo = "";
#ifdef WCOREDUMP
	if (WCOREDUMP(status)) {
		jollo = "with core dump";
	}
#endif
		printf("    signal termination %s(signal = %d)\n", jollo, WTERMSIG(status));
		return;
	}

	else {
		printf("    unknown type of termination\n");
		return;
	}
}
