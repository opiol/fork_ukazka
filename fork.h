#ifndef FORK_H
#define FORK_H

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1 /* XPG 4.2 - needed for WCOREDUMP() */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void child(void);
void grandparent(void);
void parent(void);
void print_info(const char *label);
/*void print_exit_status(pid_t pid, int status);*/
void print_exit_status(const char * label, pid_t pid, int status);
#endif
